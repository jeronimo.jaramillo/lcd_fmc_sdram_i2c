Use of LCD with SPI, FMC, I2C and SDRAM 

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 
Implement the LCD display by transmitting an image by SPI from the SDRAM and use the touch sensor
to know the coordinates of the display and how much pressure when the LCD display is pressed

PLATAFORM.
The board STM32F429ZI discovery 1 will be used on this development. 

FUNCTIONALITY.

The system was intialized and show firts the image of a star, before that the system shows an animation
animation of some circles orbiting a bigger one.  While the systems is displaying the animation, the touch
sensor is being read.